﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;

namespace Kosmos
{
    public class Sound
    {
        private AudioEngine _engine;
        private WaveBank _music;
        private SoundBank _sound;
        private Cue _track;

        public Sound()
        {
            _engine = new AudioEngine("Content/Sounds.xgs");
            _music = new WaveBank(_engine, "Content/Wave Bank.xwb");
            _sound = new SoundBank(_engine, "Content/Sound Bank.xsb");
        }

        public void play(String music)
        {
            _engine.Update();
            _track = _sound.GetCue(music);
            if (!_track.IsPlaying)
                _track.Play();
        }
    }
}
