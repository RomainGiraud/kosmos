﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;

namespace Kosmos
{
    public class CameraEffect : Camera2d
    {
        private List<Effect> _effects;

        public CameraEffect(MyGame game) : base(game)
        {
            _effects = new List<Effect>();
        }

        public Vector2 Focus
        {
            get { return Pos; }
            set { Pos = value; }
        }

        public void InitEffect(Effect effect)
        {
            _effects.Add(effect);
            effect.Initialize(this);
        }

        public void Update(GameTime gameTime)
        {
            List<Effect> removes = new List<Effect> ();

            foreach (Effect effect in _effects)
            {
                effect.Update(gameTime);  
                if (effect.isFinish) removes.Add(effect);
            }

            foreach (Effect effect in removes)
                _effects.Remove(effect);
        }
    }
}
