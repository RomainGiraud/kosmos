using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using FarseerGames.FarseerPhysics.Factories;
using FarseerGames.FarseerPhysics.Dynamics;
using System;


namespace Kosmos
{
    public class Side : AbstractBall
    {
        private Vector2 _size;
        public Vector2 Size
        {
            get { return _size; }
        }

        private Texture2D _texture;
        private String _textureName;

        public Side(MyGame game, Rectangle rect, String textureName)
            : base(game)
        {
            _size = new Vector2(rect.Width, rect.Height);
            _textureName = textureName;

            Body body = BodyFactory.Instance.CreateRectangleBody(game.Simulator, _size.X, _size.Y, /*mass*/ 1);
            // hack because position is the center of the side
            body.Position = new Vector2(rect.X + rect.Width / 2f, rect.Y + rect.Height / 2f);
            //body.Position = new Vector2(rect.X, rect.Y);
            body.IsStatic = true;
            body.LinearDragCoefficient = 1f;

            Geom = GeomFactory.Instance.CreateRectangleGeom(game.Simulator, body, _size.X, _size.Y, 1);
            Geom.CollisionCategories = getType(Type.BORDER); 
        }

        public override void Initialize()
        {
            base.Initialize();
        }

        protected override void LoadContent()
        {
            _texture = TexturesPool.GetTexture(_textureName);
            base.LoadContent();
        }

        public override void Draw(GameTime gameTime)
        {
            Rectangle rect = ((MyGame)Game).Camera.getRectangle(
                Geom.Body.Position.X,
                Geom.Body.Position.Y,
                Size.X,
                Size.Y
            );

            if (!((MyGame)Game).Camera.isVisible(rect)) return;

            SpriteBatch.Draw(_texture,
                new Microsoft.Xna.Framework.Rectangle((int)rect.X, (int)rect.Y, (int)rect.Width, (int)rect.Height),
                null, Color.White, Geom.Body.Rotation, new Vector2(_texture.Width / 2f, _texture.Height / 2f),
                SpriteEffects.None, 0);
        }
    }
}