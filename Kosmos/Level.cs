﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
using FarseerGames.FarseerPhysics.Collisions;

namespace Kosmos
{
    public class Level : Microsoft.Xna.Framework.DrawableGameComponent
    {
        /*******************************
         *           Champs            *
         *******************************/

        private FileStream fs;
        private XmlDocument xmldoc;

        // difficulté du level
        private float _difficulty;
        // dimension du level 
        private Vector2 _size;
        public Vector2 Size
        {
            get { return _size; }
        }
        
        // gestion des balles
        private int _nb_balls_init;
        private List<Ball> _balls;
        public List<Ball> ListBall
        {
            get { return _balls; }
        }
        private Ball _player_ball;
        public Ball PlayerBall
        {
            get { return _player_ball; }
        }

        private SpriteBatch _sprite;

        // texture, chemin et sprite du fond
        private Texture2D _txt2d_background;
        private String _path_background = "Background";

        // bordures
        private List<Side> _sides;

        private int SIDE_HEIGHT = 30;
        private int SIDE_WIDTH = 30;

        private float _time;
        private int _ejected;
        private bool _lock_right_click;


        /*******************************
         *        Constructeurs        *
         *******************************/
        public Level(MyGame game, int level)//, Vector2 size, float level)
            : base(game)
        {
            //_size = size;
            _difficulty = level;
            //_nb_balls_init = (int) (200f*_difficulty);
            _balls = new List<Ball>();
            //((MyGame)Game).sound.play("Star Wars");
            
        }

        //public Level(MyGame game, int width, int height, float level)
        //    : this(game, new Vector2(width, height), level)
        //{
        //}

        private void LoadFromXML()
        {
            if (_difficulty < 10)
                fs = new FileStream(Game.Content.RootDirectory + "\\level0" + _difficulty + ".xml",
                    FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
            else
                fs = new FileStream(Game.Content.RootDirectory + "\\level" + _difficulty + ".xml",
                    FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
            
            xmldoc = new XmlDocument();
            xmldoc.Load(fs);

            _txt2d_background = TexturesPool.GetTexture(xmldoc.SelectSingleNode("//level/background").InnerText);

            XmlNodeList xmlnd = xmldoc.GetElementsByTagName("size");
            _size = new Vector2(
                int.Parse(xmldoc.SelectSingleNode("//level/size/width").InnerText),
                int.Parse(xmldoc.SelectSingleNode("//level/size/height").InnerText));

            _nb_balls_init = int.Parse(xmldoc.SelectSingleNode("//level/standard_ball/number").InnerText);
        }

        /*******************************
         *      Méthodes héritées      *
         *******************************/
        public override void Initialize()
        {
            LoadFromXML();
            InitLevel();
            
            _lock_right_click = false;
            
            _sprite = ((MyGame)Game).Sprite;

            _sides = new List<Side>();
            _sides.Add(new Side(
                (MyGame)Game,
                new Rectangle(0, -SIDE_HEIGHT, (int)Size.X, SIDE_HEIGHT),
                "BorderTop"));
            _sides.Add(new Side(
                (MyGame)Game,
                new Rectangle(0, (int)Size.Y, (int)Size.X, SIDE_HEIGHT),
                "BorderBottom"));
            _sides.Add(new Side(
                (MyGame)Game,
                new Rectangle(-SIDE_WIDTH, 0, SIDE_WIDTH, (int)_size.Y),
                "BorderLeft"));
            _sides.Add(new Side(
                (MyGame)Game,
                new Rectangle((int)_size.X, 0, SIDE_WIDTH, (int)_size.Y),
                "BorderRight"));
            _sides.Add(new Side(
                (MyGame)Game,
                new Rectangle(-SIDE_WIDTH, -SIDE_HEIGHT, SIDE_WIDTH, SIDE_HEIGHT),
                "CornerTopLeft"));
            _sides.Add(new Side(
                (MyGame)Game,
                new Rectangle((int)_size.X, -SIDE_HEIGHT, SIDE_WIDTH, SIDE_HEIGHT),
                "CornerTopRight"));
            _sides.Add(new Side(
                (MyGame)Game,
                new Rectangle(-SIDE_WIDTH, (int)_size.Y, SIDE_WIDTH, SIDE_HEIGHT),
                "CornerBottomLeft"));
            _sides.Add(new Side(
                (MyGame)Game,
                new Rectangle((int)Size.X, (int)Size.Y, SIDE_WIDTH, SIDE_HEIGHT),
                "CornerBottomRight"));

            // initialisation
            foreach (Ball b in _balls)
                b.Initialize();
            foreach (Side s in _sides)
                s.Initialize();

            base.Initialize();
        }

        public override void Update(GameTime gameTime)
        {
            
            //GestionActionClavier();
            if (!((MyGame)Game).isGameOver)
                GestionActionSouris(gameTime);

            foreach (Side s in _sides)
                s.Update(gameTime);

            for (int i = 0; i < _balls.Count; )
            {
                if (_balls[i].diameter < Ball._sz_min_ball)
                {
                    if (_balls[i] == _player_ball)
                    {
                        ((MyGame)Game).isGameOver = true;
                    }

                    _balls[i].Geom.Body.Dispose();
                    _balls[i].Geom.Dispose();
                    
                    ((MyGame)Game).Simulator.Remove(_balls[i].Geom.Body);
                    ((MyGame)Game).Simulator.Remove(_balls[i].Geom);

                    ((MyGame)Game).currentLevel.ListBall.Remove(_balls[i]);
                }
                else
                {
                    _balls[i].Update(gameTime);
                    ++i;
                }
            }
        }

        public override void Draw(GameTime gameTime)
        {
            foreach (Side s in _sides)
                s.Draw(gameTime);
            foreach (Ball b in _balls)
                b.Draw(gameTime);
        }

        private void DrawBackground()
        {
            Rectangle rect = new Rectangle(
                0,
                0,
                _txt2d_background.Width,
                _txt2d_background.Height
            );

            _sprite.Draw(
                _txt2d_background,
                rect,
                Color.White
            );
        }

        /*******************************
         *       Autres méthodes       *
         *******************************/
        protected void InitLevel()
        {
            float _player_ball_diameter = 2f * float.Parse(xmldoc.SelectSingleNode("//level/player_ball/radius").InnerText);
            int player_ball_x = int.Parse(xmldoc.SelectSingleNode("//level/player_ball/x").InnerText);
            int player_ball_y = int.Parse(xmldoc.SelectSingleNode("//level/player_ball/y").InnerText);

            _player_ball = new Ball(Game, new Vector2(player_ball_x, player_ball_y), _player_ball_diameter);
            Ball.player_ball = _player_ball;

            _balls.Insert(0, _player_ball);

            _lock_right_click = false;

            XmlNodeList sun_xmln = xmldoc.SelectNodes("//level/sun_ball");
            int nb_sun_ball = sun_xmln.Count;


            List<BallSun> listSun = new List<BallSun>();
            if (nb_sun_ball > 0)
            {
                foreach (XmlNode node in sun_xmln)
                {
                    float sun_ball_diameter = 2f * float.Parse(node.SelectSingleNode("radius").InnerText);
                    int sun_ball_x = int.Parse(node.SelectSingleNode("x").InnerText);
                    int sun_ball_y = int.Parse(node.SelectSingleNode("y").InnerText);
                    BallSun sun = new BallSun(Game, new Vector2(sun_ball_x, sun_ball_y), sun_ball_diameter);
                    _balls.Insert(0, sun);
                    listSun.Add(sun);
                }
            }

            XmlNodeList xmln = xmldoc.SelectNodes("//level/green_ball");
            int nb_green_ball = xmln.Count;
            if (nb_green_ball > 0)
            {
                foreach(XmlNode node in xmln)
                {
                    float green_ball_diameter = 2f * float.Parse(node.SelectSingleNode("radius").InnerText);
                    int green_ball_x = int.Parse(node.SelectSingleNode("x").InnerText);
                    int green_ball_y = int.Parse(node.SelectSingleNode("y").InnerText);
                    BallGreen green = new BallGreen(Game, new Vector2(green_ball_x, green_ball_y), green_ball_diameter);
                    _balls.Insert(0, green);
                }
            }
            //_balls.Insert(0,new BallGreen(Game, new Vector2(500, 260), 50));

            float min_diameter = float.Parse(xmldoc.SelectSingleNode("//level/standard_ball/size_min").InnerText);
            float max_diameter = float.Parse(xmldoc.SelectSingleNode("//level/standard_ball/size_max").InnerText);

            for (int i = 0; i < _nb_balls_init; ++i)
            {
                Vector2 pos;
                float diameter;
                bool IsInCollision;
                do
                {
                    IsInCollision = true;

                    float x = (float)(MyGame.random.NextDouble() * Size.X);
                    float y = (float)(MyGame.random.NextDouble() * Size.Y);
                    pos = new Vector2(x, y);
                    diameter = min_diameter + (float)(Math.Pow(MyGame.random.NextDouble(), 2) * max_diameter);
                    //diameter = 20;
                    if (x < (diameter / 2f) || y < (diameter / 2f))
                        continue;

                    bool IsInCollisionWithBall = false;
                    // verification de collision avec la balle du joueur
                    if (x + (diameter / 2f) >= _player_ball.left &&
                        x - (diameter / 2f) <= _player_ball.right &&
                        y + (diameter / 2f) >= _player_ball.top &&
                        y - (diameter / 2f) <= _player_ball.bottom)
                    {
                        continue;
                    }

                    foreach (Ball ball in _balls)
                    {
                        // verification de collision avec les balles deja créé
                        if (x + (diameter / 2f) >= ball.left
                            && x - (diameter / 2f) <= ball.right
                            && y + (diameter / 2f) >= ball.top
                            && y - (diameter / 2f) <= ball.bottom)
                        {
                            IsInCollisionWithBall = true;
                            break;
                        }
                    }

                    // verification que la balle ne deborde pas du jeu
                    IsInCollision = (x + (diameter / 2f) >= Size.X) ||
                                    (y + (diameter / 2f) >= Size.Y) ||
                                    IsInCollisionWithBall;
                }
                while (IsInCollision);

                _balls.Insert(0, new Ball(Game, pos, diameter));
                //Game.Components.Add(_balls[i]);
            }

            // On met en orbite les balles avec le soleil, si il y en a un

            foreach (BallSun sun in listSun)
                foreach (Ball b in _balls)
                {
                    if (sun == b) continue;

                    sun.MettreEnOrbite(b);
                }
            
        }

        

        // clique de la souris
        protected void GestionActionSouris(GameTime gameTime)
        {
            if (ButtonState.Pressed == Mouse.GetState().RightButton)
            {
                if (!_lock_right_click)
                {
                    ((MyGame)Game).timeValue = (((MyGame)Game).timeValue == 1) ? 0.125f : 1f;
                    _lock_right_click =true;
                }
            }
            if (ButtonState.Released == Mouse.GetState().RightButton)
            {
                _lock_right_click = false;
            }

            _time += gameTime.ElapsedGameTime.Milliseconds;
            if (ButtonState.Pressed == Mouse.GetState().LeftButton)
            {
                int tmp = (int)(_time - gameTime.ElapsedGameTime.Milliseconds * ((MyGame)Game).timeValue) / 500;
                if (tmp > _ejected)
                {
                    _ejected = tmp;
                
                    int x = Mouse.GetState().X;
                    int y = Mouse.GetState().Y;
                    float oX = ((MyGame)Game).GraphicsDeviceManager.PreferredBackBufferWidth / 2f;
                    float oY = ((MyGame)Game).GraphicsDeviceManager.PreferredBackBufferHeight / 2f;

                    float angle = (float)Math.Atan2(y - oY, x - oX);
                    angle += (float)Math.PI;
                    Vector2 vec = new Vector2((float)Math.Cos(angle), (float)Math.Sin(angle));
                    vec *= _player_ball.Geom.Body.Mass * 10f;

                    _player_ball.Geom.Body.ApplyImpulse(vec);

                    //angle = (float)Math.Atan2(-vec.Y, -vec.X);
                    
                    angle += (float)Math.PI;
                    
                    float aire = _player_ball.aire * Ball._percent_eject_when_move;

                    _player_ball.aire -= aire;
                    //_player_ball.Geom.Body.Position = new Vector2(_player_ball.Geom.Position.X, _player_ball.Geom.Position.Y);
                                                            
                    float diameter = (float)Math.Sqrt (aire / Math.PI)*2f;

                    float dist = (_player_ball.diameter + diameter) / 2;
                    float xb = (float)(Math.Cos(angle) * dist + _player_ball.centerX);
                    float yb = (float)(Math.Sin(angle) * dist + _player_ball.centerY);
                    
                    Ball b = new Ball(Game, new Vector2 (xb,yb), diameter);
                    /*
                    Vector2 vec2 = _player_ball.Geom.Body.GetVelocityAtLocalPoint(_player_ball.Geom.Body.Position);//new Vector2(-vec.X, -vec.Y);
                    vec2 *= b.Geom.Body.Mass / _player_ball.Geom.Body.Mass;
                    b.Geom.Body.ApplyImpulse(vec2);
                    */
                    Vector2 vec2 = new Vector2(-vec.X, -vec.Y);
                    vec2 *= 0.5f;
                    b.Geom.Body.ApplyImpulse(vec2);
                    
                    _balls.Insert(0, b);
                }
            }
            else if (ButtonState.Released == Mouse.GetState().LeftButton)
            {
                _time = 0f;
                _ejected = -1;
            }
        }
    }
}