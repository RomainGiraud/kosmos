﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;

namespace Kosmos
{
    public class EffectProgressiveZoom : Effect
    {
        private float _zoom;
        private float _zoom_done;

        public EffectProgressiveZoom(int time, float final_zoom)
            :base (time)
        {
            _zoom = final_zoom;
            _zoom_done = 0f;
        }

        public override void Initialize(Camera2d camera)
        {
            base.Initialize(camera);
            _zoom -= _camera.Zoom;
        }

        public override void Update(GameTime gameTime)
        {
            float zoom = (float)((double)_zoom *
                ((double)gameTime.ElapsedGameTime.Milliseconds
                / (double)_time));

            _zoom_done += zoom;

            if (_zoom > 0 && _zoom_done > _zoom)
            {
                _is_finish = true;
                zoom -= _zoom_done - _zoom;
            }
            else if (_zoom < 0 && _zoom_done < _zoom)
            {
                _is_finish = true;
                zoom += _zoom_done - _zoom;
            }

            _camera.Zoom += zoom;
        }
    }
}
