﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;

namespace Kosmos
{
    public class Camera2d
    {
        private float _zoom_scale; // Camera Zoom
        private float _zoom; // Object Zoom
        private Matrix _transform; // Matrix Transform
        private Vector2 _pos; // Camera Position
        protected float _rotation; // Camera Rotation
        protected MyGame _game;

        public Camera2d(MyGame game)
        {
            _zoom = 1f;
            _zoom_scale = 1.0f;
            _rotation = 0.0f;
            _pos = Vector2.Zero;
            //_pos = new Vector2(MyGame._max_width/2.0f, MyGame._max_height/2.0f);
            _game = game;
        }

        // Sets and gets zoom
        public float Zoom
        {
            get { return _zoom; }
            set 
            { 
                _zoom = value; 
                if (_zoom < 0.5f) _zoom = 0.5f; 
                if (_zoom > 20.0f) _zoom = 20.0f; 
            } 
        }

        // Auxiliary function to move the camera
        public void Move(Vector2 amount)
        {
            _pos += amount;
        }

        // Get set position
        public Vector2 Pos
        {
            get { return _pos; }
            set { _pos = value; }
        }

        // Get set rotation
        public float Rotation
        {
            get { return _rotation; }
            set { _rotation = value; }
        }

        public Matrix getTransformation(GraphicsDevice graphicsDevice)
        {
            _transform = Matrix.CreateTranslation (new Vector3(-_pos.X, -_pos.Y, 0)) 
                * Matrix.CreateRotationZ(Rotation)
                * Matrix.CreateScale(new Vector3(_zoom_scale, _zoom_scale, 0)) 
                * Matrix.CreateTranslation
                  (
                     new Vector3
                     (
                        ((MyGame)_game).GraphicsDeviceManager.PreferredBackBufferWidth * 0.5f,
                        ((MyGame)_game).GraphicsDeviceManager.PreferredBackBufferHeight * 0.5f, 
                        0
                     )
                  );

            return _transform;
        }

        public Rectangle getRectangle(float x, float y, float width, float height)
        {
            Rectangle result = new Rectangle();
            result.X = (int)(x * Zoom);
            result.Y = (int)(y * Zoom);
            result.Width = (int)(width * Zoom);
            result.Height = (int)(height * Zoom);
            return result;
        }

        public bool isVisible(Rectangle rec)
        {
            float diag = (float)Math.Sqrt(Math.Pow(MyGame.WINDOW_WIDTH, 2) + Math.Pow(MyGame.WINDOW_HEIGHT, 2))/2f;

            return !(_pos.X + diag < rec.X - rec.Width / 2f
                || _pos.Y + diag < rec.Y - rec.Height / 2f
                || _pos.X - diag > rec.X + rec.Width / 2f
                || _pos.Y - diag > rec.Y + rec.Height / 2f);
        }

    }
}
