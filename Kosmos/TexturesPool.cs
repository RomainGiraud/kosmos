using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;


namespace Kosmos
{
    public class TexturesPool
    {
        private TexturesPool() {}
        private static Game _game;
        private static Dictionary<String, Texture2D> _dictionary;
        public static Texture2D GetTexture(String name)
        {
            if (_game == null) throw new Exception("TexturesPool not initiliazed - use TexturesPool.Initialize(Game)");
            if (!_dictionary.ContainsKey(name)) _dictionary.Add(name, _game.Content.Load<Texture2D>(name));
            return _dictionary[name];
        }
        public static void Initialize(Game game)
        {
            _game = game;
            _dictionary = new Dictionary<String, Texture2D>();
        }
    }
}