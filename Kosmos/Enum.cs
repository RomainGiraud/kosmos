﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Kosmos
{
    public enum Type
    {
        BALL = FarseerGames.FarseerPhysics.CollisionCategory.Cat1,
        SUN = FarseerGames.FarseerPhysics.CollisionCategory.Cat3,
        GREEN = FarseerGames.FarseerPhysics.CollisionCategory.Cat4,
        BORDER = FarseerGames.FarseerPhysics.CollisionCategory.Cat2,
    }
}
