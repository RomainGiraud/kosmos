﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;
using FarseerGames.FarseerPhysics.Factories;
using FarseerGames.FarseerPhysics.Collisions;
using FarseerGames.FarseerPhysics.Dynamics;

namespace Kosmos
{
    class BallGreen : Ball
    {

        public BallGreen(Game g, Vector2 position, float diameter)
            : base (g, position, diameter)
        {
            Geom.CollisionCategories = getType(Type.GREEN);
            _type_ball = Type.GREEN;
        }

        protected override Color getColor()
        {
            return new Color(0, 255, 0);
        }

    }
}
