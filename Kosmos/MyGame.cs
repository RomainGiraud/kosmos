using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Xml;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;
using FarseerGames.FarseerPhysics;

namespace Kosmos
{
    public class MyGame : Microsoft.Xna.Framework.Game
    {
        public const int WINDOW_WIDTH = 1280;
        public const int WINDOW_HEIGHT = 1024;
        public static System.Random random = new Random();


        private float _update_time;
        public float UpdateTime
        {
            get { return _update_time; }
        }


        private bool _isGameOver;
        public bool isGameOver
        {
            get { return _isGameOver; }
            set { _isGameOver = value; }
        }

        private GraphicsDeviceManager _graphics;
        public GraphicsDeviceManager GraphicsDeviceManager
        {
            get { return _graphics; }
        }

        private SpriteBatch _spriteBatch;
        public SpriteBatch Sprite
        {
            get { return _spriteBatch; }
        }

        private CameraEffect _camera;
        public CameraEffect Camera
        {
            get { return _camera; }
        }

        private Player _player;
        private float _scroll;
        private float _time;

        private float _time_value;
        public float timeValue
        {
            get { return _time_value; }
            set { _time_value = value; }
        }


        private PhysicsSimulator _simulator;
        public PhysicsSimulator Simulator
        {
            get { return _simulator; }
        }

        private Sound _sound;
        public Sound sound
        {
            get { return _sound; }
        }

        List<Level> _levels;
        private Level _current;
        public Level currentLevel
        {
            get { return _current; }
        }

        public MyGame()
        {
            _graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";

            _scroll = Mouse.GetState().ScrollWheelValue;

            _simulator = new PhysicsSimulator(new Vector2(0, 0));

            _time_value = 1f;
            _time = 0f;
            _levels = new List<Level>();
            //_sound = new Sound();
        }

        protected override void Initialize()
        {
            IsMouseVisible = true;

            _graphics.PreferredBackBufferWidth = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Width;
            _graphics.PreferredBackBufferHeight = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Height;
            _graphics.PreferredBackBufferFormat = SurfaceFormat.Bgr32;
            _graphics.IsFullScreen = true;
            _graphics.ApplyChanges();

            _spriteBatch = new SpriteBatch(GraphicsDevice);

            AbstractBall.SpriteBatch = _spriteBatch;

            TexturesPool.Initialize(this);


            _camera = new CameraEffect(this);
            //_camera.InitEffect(new EffectProgressiveZoom (5000, 10f));

            // niveaux
            DirectoryInfo levelDir = new DirectoryInfo(Content.RootDirectory);
            foreach (FileInfo file in levelDir.GetFiles())
                _levels.Add(new Level(this, 3));//, file.FullName));
            _current = _levels[0];
            Components.Add(_current);

            _player = new Player(this, _current.PlayerBall);

            base.Initialize();
        }

        protected override void Update(GameTime gameTime)
        {
            if (Keyboard.GetState().IsKeyDown(Keys.Escape))
                this.Exit();

            _update_time = (float)gameTime.ElapsedGameTime.Milliseconds * 0.001f * _time_value;
            _simulator.Update(_update_time);

            _player.Update(gameTime);

            base.Update(gameTime);

            GestionActionClavier(gameTime);
            GestionCamera(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            _graphics.GraphicsDevice.Clear(new Color(0, 0, 0));

            _spriteBatch.Begin(
                SpriteBlendMode.AlphaBlend,
                SpriteSortMode.Immediate,
                SaveStateMode.SaveState,
                Camera.getTransformation(_spriteBatch.GraphicsDevice)
            );

            base.Draw(gameTime);
            _spriteBatch.End();
            
            //_currentLevel.DrawBackground();
        }

        protected void GestionActionClavier(GameTime gameTime)
        {
            if (_time < 100)
            {
                _time += gameTime.ElapsedGameTime.Milliseconds;
                return;
            }
            _time -= 100;

            Keys[] keys = Keyboard.GetState().GetPressedKeys();

            if (keys.Contains(Keys.Up))
            {
                float tmp = (_camera.Zoom) * 1.2f;
                Camera.InitEffect(new EffectProgressiveZoom(500, tmp));
            }
            if (keys.Contains(Keys.Down))
            {
                float tmp = (_camera.Zoom) / 1.1f;
                Camera.InitEffect(new EffectProgressiveZoom(500, tmp));
            }
            if (keys.Contains(Keys.Left))
            {
                _time_value = 0.125f;
            }
            if (keys.Contains(Keys.Right))
            {
                _time_value = 1f;
            }            
        }

        protected void GestionCamera(GameTime gameTime)
        {
            if (_scroll != Mouse.GetState().ScrollWheelValue)
            {
                float tmp = ((_scroll <= Mouse.GetState().ScrollWheelValue) ? (_camera.Zoom) * 1.2f : (_camera.Zoom) / 1.1f);
                _camera.InitEffect(new EffectProgressiveZoom(500, tmp));
                //_current.player_ball.diameter += 0.25f;

                _scroll = Mouse.GetState().ScrollWheelValue;
            }
                        
            _camera.Update(gameTime);

            if (_camera.Zoom > 300 / _current.PlayerBall.diameter)
                _camera.Zoom = 300 / _current.PlayerBall.diameter;

            _camera.Focus = _current.PlayerBall.center * _camera.Zoom;
        }
    }
}
