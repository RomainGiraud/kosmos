﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;

namespace Kosmos
{
    class EffectProgressiveRotation : Effect
    {
        private float _rotation;
        private float _rotation_done;

        public EffectProgressiveRotation(int time, float final_rotation)
            :base (time)
        {
            _rotation = final_rotation;
            _rotation_done = 0f;
        }

        public override void Initialize(Camera2d camera)
        {
            base.Initialize(camera);
            _rotation -= - _camera.Rotation;
        }

        public override void Update(GameTime gameTime)
        {
            float rotation = (float) ((double)_rotation * 
                ((double)gameTime.ElapsedGameTime.Milliseconds 
                / (double)_time));

            _rotation_done += rotation;

            if (_rotation > 0 && _rotation_done > _rotation)
            {
                _is_finish = true;
                rotation -= _rotation_done - _rotation;
            }
            else if (_rotation < 0 && _rotation_done < _rotation)
            {
                _is_finish = true;
                rotation += _rotation_done - _rotation;
            }

            _camera.Rotation += rotation;
        }
    }
}
