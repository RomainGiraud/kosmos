using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;

using FarseerGames.FarseerPhysics.Collisions;
using FarseerGames.FarseerPhysics.Dynamics;


namespace Kosmos
{
    public class AbstractBall : DrawableGameComponent
    {
        protected Geom _geom;
        protected Body _body;

        public Geom Geom
        {
            get { return _geom; }
            set
            {
                _geom = value;
                _geom.CollisionResponseEnabled = true;
                _geom.RestitutionCoefficient = 1f;
                _geom.FrictionCoefficient = 1f;
            }
        }

        public FarseerGames.FarseerPhysics.CollisionCategory getType(Type t)
        {
            return (FarseerGames.FarseerPhysics.CollisionCategory)t;
        }

        private static SpriteBatch _spriteBatch;
        public static SpriteBatch SpriteBatch
        {
            get { return _spriteBatch; }
            set { _spriteBatch = value; }
        }


        public AbstractBall(Game game)
            : base(game)
        {
        }
    }
}