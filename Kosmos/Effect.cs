﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;

namespace Kosmos
{
    public class Effect
    {
        protected int _time;
        protected Camera2d _camera;
        protected bool _is_finish;

        public Effect(int time)
        {
            _time = time;
            _is_finish = false;
        }

        public virtual void Initialize(Camera2d camera)
        {
            _camera = camera;
        }

        public virtual void Update(GameTime gameTime)
        {
        }

        public bool isFinish
        {
            get { return _is_finish; }
        }
    }
}
