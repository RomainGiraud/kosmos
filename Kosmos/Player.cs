﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;

namespace Kosmos
{
    public class Player
    {
        private MyGame _game;
        private Ball _player_ball;
        //private Profile _profile;

        public Player(MyGame game, Ball player_ball)
        {
            _game = game;
            _player_ball = player_ball;
        }

        public void Update(GameTime gameTime)
        {
        }
    }
}
